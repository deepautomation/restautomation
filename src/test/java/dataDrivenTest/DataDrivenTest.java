package dataDrivenTest;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class DataDrivenTest {
	
	
	@Test(dataProvider="empdataprovider")
	void PostNewData(String title, String userId) {
		

		RestAssured.baseURI="https://jsonplaceholder.typicode.com/posts";
		
		// Request object
				RequestSpecification httprequest = RestAssured.given();
				
				
				
				// created data that we can send to post request
				JSONObject requestparams = new JSONObject();
				requestparams.put("title", title);
				requestparams.put("userId", userId);
				
				// add header stating the request body is json
				httprequest.header("content-type","application/json");
				
				// data should go as a json format. by default it is in hashmap format
				httprequest.body(requestparams.toJSONString());
				
				//POST request
			Response response =	httprequest.request(Method.POST);
				
				// Capture the response body to perform validation
			String responsebody = response.getBody().asString();
			
			System.out.println(responsebody);
	//		Assert.assertEquals(responsebody.contains("abc"),true);
			
			int sc = response.statusCode();
			System.out.println(sc); 
			
	}

	@DataProvider(name = "empdataprovider")
	String [][] getEmpData() 
	{
		
		String empdata[][] = {{"abc","2110"},{"xyz","2111"}};
		
		return(empdata);
	}
}
