package restfulBooker;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class GetBooking {

	@Test
	public void GetBooking() {
		
		// Build request
		
		 RestAssured
		 .given()
		 	.log()
		 	.all()
		 	.baseUri("https://restful-booker.herokuapp.com/")
		 	.basePath("booking/{id}")
		 	.pathParam("id", 1)
		 	
		//hit the request and get repsonse
		
		 .when() //BDD format
		 	.get()
		
		// Validate the response
		.then()
			.log()
			.all()
			.statusCode(200);
			
	}
	
}
