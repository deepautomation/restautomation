package restfulBooker;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class CreateBooking {

	@Test
	public void postBooking() {

		// Build the Request
//		
//		RequestSpecification requestSpecification = RestAssured.given();
//		requestSpecification = requestSpecification.log().all();
//		requestSpecification.baseUri("https://restful-booker.herokuapp.com/");
//		requestSpecification.basePath("booking");

		//Setup/Configure the request
		RestAssured
		.given()
		.log()
		.all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
				.body("{\r\n" + "    \"firstname\" : \"Amod\",\r\n" + "    \"lastname\" : \"White\",\r\n"
						+ "    \"totalprice\" : 9999,\r\n" + "    \"depositpaid\" : true,\r\n"
						+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2021-01-01\",\r\n"
						+ "        \"checkout\" : \"2022-01-01\"\r\n" + "    },\r\n"
						+ "    \"additionalneeds\" : \"Breakfast\"\r\n" + "}")

		.contentType(ContentType.JSON)
		
		// Hit the request and get the response
		.post()
		.then() // Validate the repsonse
		.log()
		.all()
		.statusCode(200);

		// Hit the Request and Get Response
//		Response response = requestSpecification.post();

		// Validate Response
//		ValidatableResponse validate = response.then().log().all();
//		validate.statusCode(200);
	}
}
