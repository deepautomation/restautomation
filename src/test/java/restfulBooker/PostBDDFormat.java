package restfulBooker;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PostBDDFormat {

	@Test
	public void createBooking() {
		
		// Build Request = GIVEN
		RestAssured
			.given()
				.log()
				.all()
				.baseUri("https://restful-booker.herokuapp.com/")
				.basePath("booking")
				.body("{\r\n" + "    \"firstname\" : \"Amod\",\r\n" + "    \"lastname\" : \"White\",\r\n"
							+ "    \"totalprice\" : 9999,\r\n" + "    \"depositpaid\" : true,\r\n"
							+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2021-01-01\",\r\n"
							+ "        \"checkout\" : \"2022-01-01\"\r\n" + "    },\r\n"
							+ "    \"additionalneeds\" : \"Breakfast\"\r\n" + "}")

				.contentType(ContentType.JSON)
			.when()
				.post()
			.then()
				.log()
				.all()
				.statusCode(200);
	}
}
