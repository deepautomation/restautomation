package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	
	
	public static void main(String[] args) throws IOException {
		
		getRowCount();
		getCellData();
	}
	
	
	public static void getCellData() throws IOException {
		
		String excelPath = "./data/TestData.xlsx";
		XSSFWorkbook workbook = new XSSFWorkbook(excelPath);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		
		String value = sheet.getRow(1).getCell(0).getStringCellValue();
		System.out.println(value);
		
	}
	
	public static void getRowCount() throws IOException {
		
		String excelPath = "./data/TestData.xlsx";
		XSSFWorkbook workbook = new XSSFWorkbook(excelPath);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		int rowcount = sheet.getPhysicalNumberOfRows();
		
		System.out.println("Number of rows " +rowcount);
	}

}
