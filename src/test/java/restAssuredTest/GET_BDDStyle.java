package restAssuredTest;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class GET_BDDStyle {

	@Test
	public void GetBookingIds_VerifyStatusCode() {
		
		//Given
		RestAssured.given()
		    .baseUri("https://restful-booker.herokuapp.com")
		    
		// When
		    .when()
		       .get("/booking")
		// Then
		    .then()
		         .statusCode(200)
		         .statusLine("HTTP/1.1 200 OK")
		         // To verify booking count
	//	         .body("bookingid.sum()", Matchers.hasSize(55))
		         .body("bookingid", Matchers.hasItem(7)).log().all()
		         
		         // To verify booking id at index 3
		         .body("bookingid[3]", Matchers.equalTo(7));
		
	}
}
