package restAssuredTest;

import org.testng.annotations.Test;

import com.google.inject.matcher.Matchers;

import io.restassured.response.Response;

import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.*;

public class GETBDDStyle_StaticImport {

	@Test
	public void GetBookingIdsVerifyStatusCode() {
		
		given()
		    .baseUri("https://restful-booker.herokuapp.com")
		    
		.when()
		     .get("/booking")
		     
	    .then()
		    .statusCode(200)
		    .statusLine("HTTP/1.1 200 OK")
		    .log().all();
	//	    .body("bookingid", contains(10));
		    
		
		
		
	}
}
