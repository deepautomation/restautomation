package restAssuredTest;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class NonBDDPatchRequest {

	@Test
	public void UpdateUserDetails() {
		
		String jsonString = "{\r\n" + 
				"    \"firstname\" : \"Deepak\",\r\n" + 
				"    \"lastname\" : \"Bhardwaj\"}";
		
		RequestSpecification request = RestAssured.given();
		
		request.contentType(ContentType.JSON);
		request.cookie("token", "08822ec7f9fbcc0");
		request.baseUri("https://restful-booker.herokuapp.com/booking/1");
		
		request.body(jsonString);
		
		Response response = request.patch();
		
		System.out.println(response.asString());
		
		ValidatableResponse validate = response.then();
		
		validate.statusCode(200);
		validate.body("firstname", Matchers.equalTo("Deepak"));
		validate.body("lastname", Matchers.equalTo("Bhardwaj"));
	}
}
