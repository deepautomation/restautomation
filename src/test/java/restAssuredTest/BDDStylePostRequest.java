package restAssuredTest;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class BDDStylePostRequest {

	@Test
	public void PostRequestToken() {
		
		String jsonString = "{\"username\" : \"admin\",\"password\" : \"password123\"}";
		
		RestAssured
		   		.given()
		   			.baseUri("https://restful-booker.herokuapp.com/auth")
		   			.contentType(ContentType.JSON)
		   			.body(jsonString)
		   		.when()
		   			.post()
		   		.then().log().all()
		   			.assertThat()
		   			.statusCode(200)
		   			.body("token", Matchers.notNullValue())
		   			.body("token.length()", Matchers.notNullValue());
		   	//		.body("token", Matchers.containsString("c"));
	}
}
