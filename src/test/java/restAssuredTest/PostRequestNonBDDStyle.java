package restAssuredTest;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class PostRequestNonBDDStyle {

	@Test
	public void RequestToken() {
		
	    String jsonString = "{\"username\" : \"admin\",\"password\" : \"password123\"}";
		
		RequestSpecification request = RestAssured.given();
		
		request.contentType(ContentType.JSON);
		request.baseUri("https://restful-booker.herokuapp.com/auth");
		request.body(jsonString);
		
		Response response = request.post();
		System.out.println("Response: " +response.asString());
		
		ValidatableResponse validate = response.then();
		validate.statusCode(200);
		
		validate.body("token.length()", Matchers.is(15));
		validate.body("token", Matchers.notNullValue());
		
		
	}
}
