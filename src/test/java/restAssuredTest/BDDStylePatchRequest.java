package restAssuredTest;

import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class BDDStylePatchRequest {

	@Test
	public void UpdateUserDetails() {
		
		String jsonString = "{\r\n" + 
				"    \"firstname\" : \"Deepak\",\r\n" + 
				"    \"lastname\" : \"Mahajan\"}";
		
		RestAssured
				.given()
						.baseUri("https://restful-booker.herokuapp.com/booking/1")
						.cookie("token", "ada37da968eb4b7")
						.contentType(ContentType.JSON)
						.body(jsonString)
				
				.when()
						.patch()
				
				.then()
						.log()
						.all()	
								.assertThat()
								.statusCode(200)
								.body("firstname", equalTo("Deepak"));
						
	}
}
