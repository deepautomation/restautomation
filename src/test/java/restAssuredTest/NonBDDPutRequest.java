package restAssuredTest;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class NonBDDPutRequest {

	@Test
	public void updateUsers() {
		
		String jsonString = "{\r\n" + "    \"firstname\" : \"Deepak\",\r\n" + "    \"lastname\" : \"Bhardwaj\",\r\n"
				+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n" + "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2018-01-01\",\r\n" + "        \"checkout\" : \"2019-01-01\"\r\n"
				+ "    },\r\n" + "    \"additionalneeds\" : \"Breakfast\"\r\n" + "}";
 
		RequestSpecification request = RestAssured.given();
		
		request.contentType(ContentType.JSON);
		request.cookie("token", "dbd09431308dff8");
		request.baseUri("https://restful-booker.herokuapp.com/booking/1");
		request.body(jsonString);
		
		Response response = request.put();
		
		System.out.println("Response: " + response.asString());
		
		ValidatableResponse validate = response.then();
		validate.statusCode(200);
		validate.body("firstname", Matchers.equalTo("Deepak"));
	}
}
