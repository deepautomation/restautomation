package Restassuredautomation.Restassuredautomation;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import io.restassured.http.ContentType;

public class Tests_POST {

	@Test
	public void test_01() {
		
		
		Map<String, Object> map = new HashMap<String, Object>();
		
//		map.put("name", "Deepak");
//		map.put("Job", "Automation QA");
		
//		System.out.println(map);
		
		JSONObject request = new JSONObject();
		
		request.put("name", "Deepak");
		request.put("Job", "Automation QA");
		request.put("name 1", "Deepak Bhardwaj");
		request.put("Post", "QA Engineer");
		
		System.out.println(request);
		
		given()
		.header("Content-Type", "application/json")
		.contentType(ContentType.JSON)
		.accept(ContentType.JSON)
		.body(request.toJSONString())
		.when()
		.post("https://reqres.in/api/users")
		.then()
		.statusCode(201);
	}
	
}
