package Restassuredautomation.Restassuredautomation;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.matcher.RestAssuredMatchers;

import org.apache.tools.ant.taskdefs.condition.Equals;
import static org.hamcrest.core.IsEqual.*;
import org.hamcrest.Matcher.*;

public class TC003_GET_Request {

	@Test
	void getUserList() {
		
//		Response response = RestAssured.get("https://reqres.in/api/users?page=2");
		
		Response response = get("https://reqres.in/api/users?page=2");
		String body = response.getBody().asString();
		int statuscode = response.getStatusCode();
		
		System.out.println("Response Body = " +body);
		System.out.println("Status Code = " +statuscode);
		System.out.println(response.getStatusLine());
		
		Assert.assertEquals(statuscode, 200);
	
	}
	@Test
	void getUserList_2() {
		
		given()
		.get("https://reqres.in/api/users?page=2")
		.then()
		.statusCode(200)
		.body("data.id[0]", equalTo(7));
	}
}

