package Restassuredautomation.Restassuredautomation;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TC001_POST_Request {
	

	@Test
	void resgisterDetails() {
		
		//Specify base URI
		
		RestAssured.baseURI="https://reqres.in/api/";
		
		// Request object
		RequestSpecification httprequest = RestAssured.given();
		
		
		//Request Payload
		JSONObject requestParams=new JSONObject();
		
		requestParams.put("name", "morpheus");
		requestParams.put("job", "leader");
		
		httprequest.headers("Content-Type", "application/json");
		
		// attach above data to request
		httprequest.body(requestParams.toJSONString());
		
		// Response
		Response response = httprequest.request(Method.POST,"/users");
		
		
		//print response in console window
		String responsebody = response.getBody().asString();
		System.out.println(responsebody);
		
		
		//status code validation
		int statuscode = response.statusCode();
		System.out.println(statuscode);
	    Assert.assertEquals(statuscode, 201);
	    
	    String idvalue = response.jsonPath().get("id");
	    System.out.println("Id is: " +idvalue);
	    
	//    Assert.assertEquals(idvalue, 423);
	}

}
