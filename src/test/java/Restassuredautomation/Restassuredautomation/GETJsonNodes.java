package Restassuredautomation.Restassuredautomation;

import org.junit.Assert;
import static io.restassured.RestAssured.*;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GETJsonNodes {

	@Test
	public void getWeatherDetails() {
		
		//Specify base URI
		
		RestAssured.baseURI="https://reqres.in/";
		
		String requestBody = "{\r\n" + 
				"    \"name\": \"morpheus\",\r\n" + 
				"    \"job\": \"leader\"\r\n" + 
				"}";
		
		Response resp = given().body(requestBody)
		.when().post("api/users")
		.then().assertThat().statusCode(201).log().all().
		extract().response();
		
    	String response = resp.asString();
		
    	JsonPath jsonresponse = new JsonPath(response);
    	String rid = jsonresponse.get("id");
    	String rname = jsonresponse.get("name");
		
		System.out.println(rid);
		System.out.println(rname);
		
		
	}	
}
