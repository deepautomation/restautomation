package Restassuredautomation.Restassuredautomation;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;

public class Tests_MockAPI {

//	@Test
	public void test_get() {
		
		RestAssured.baseURI = "http://localhost:3000/";
		
		given()
		.get("/users")
		.then()
		.statusCode(200)
		.log().all();
		
		
	}
	
//	@Test
	public void test_getParam() {
		
		given()
		.param("name", "Automation")
		.get("/subjects")
		.then()
		.statusCode(200)
		.log().all();
		
	}
	
//	@Test
	public void test_post() {
		
		RestAssured.baseURI = "http://localhost:3000/";
		JSONObject request = new JSONObject();
		
		request.put("first_name", "Tom");
		request.put("last_name", "Cooper");
		request.put("subjectId", "1");
		
		given()
		.contentType(ContentType.JSON)
		.accept(ContentType.JSON)
		.header("Content-Type", "application/json")
		.body(request.toJSONString())
		.when()
		.post("/users")
		.then()
		.statusCode(201)
		.log().all();
	}
	
	
//	@Test
	public void test_patch() {
		
		RestAssured.baseURI = "http://localhost:3000/";
		JSONObject request = new JSONObject();
		
		request.put("last_name", "Shefard");
		
		given()
		.contentType(ContentType.JSON)
		.accept(ContentType.JSON)
		.header("Content-Type", "application/json")
		.body(request.toJSONString())
		.when()
		.patch("/users/4")
		.then()
		.statusCode(200)
		.log().all();
	}
	
	@Test
	public void test_put() {
		
		RestAssured.baseURI = "http://localhost:3000/";
		JSONObject request = new JSONObject();
		
		request.put("first_name", "Jorge");
		request.put("last_name", "Arellano");
		request.put("subjectId", "1");
		
		given()
		.contentType(ContentType.JSON)
		.accept(ContentType.JSON)
		.header("Content-Type", "application/json")
		.body(request.toJSONString())
		.when()
		.put("/users/4")
		.then()
		.statusCode(200)
		.log().all();
	}
	
	@Test
	public void test_delete() {
		
		RestAssured.baseURI = "http://localhost:3000/";
		
		when()
		.delete("/users/4")
		.then()
		.statusCode(200);
	}
}
