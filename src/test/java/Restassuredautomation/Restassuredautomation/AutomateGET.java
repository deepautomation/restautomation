package Restassuredautomation.Restassuredautomation;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class AutomateGET {


    @org.testng.annotations.Test
    public void validate_get_status_code(){

        given()
                .baseUri("https://api.postman.com").
                header("X-Api-Key", "PMAK-60c8d648d0a51f003bf149fb-f8b39cc95e2cc9d948262f3967f4724f7e").
         when()
                .get("/workspaces").
          then().
                log().all().
                assertThat().
                statusCode(200);
    }

}
