package Restassuredautomation.Restassuredautomation;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class DataDrivenTest extends DataForTests {
	

	
//	@Test(dataProvider = "DataForPost")
	public void test_post(String first_name, String last_name, int subjectId) {
		
		RestAssured.baseURI = "http://localhost:3000/";
		JSONObject request = new JSONObject();
		
		request.put("first_name", first_name);
		request.put("last_name", last_name);
		request.put("subjectId", subjectId);
		
		given()
		.contentType(ContentType.JSON)
		.accept(ContentType.JSON)
		.header("Content-Type", "application/json")
		.body(request.toJSONString())
		.when()
		.post("/users")
		.then()
		.statusCode(201)
		.log().all();
	}

	
	
//	@Test (dataProvider = "DeleteData")
	public void test_delete(int id) {
		
		RestAssured.baseURI = "http://localhost:3000/";
		
		when()
		.delete("/users/" +id)
		.then()
		.statusCode(200);
	}
	
	@Parameters ({"id"})
	@Test (dataProvider = "DeleteData")
	public void test_delete2(int id) {
		
		System.out.println("Value for id is " +id);
		RestAssured.baseURI = "http://localhost:3000/";
		
		when()
		.delete("/users/" +id)
		.then()
		.statusCode(200);
	}
}
