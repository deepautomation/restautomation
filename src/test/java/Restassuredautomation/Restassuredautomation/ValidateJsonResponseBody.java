package Restassuredautomation.Restassuredautomation;

import org.junit.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ValidateJsonResponseBody {

	@Test
	public void getWeatherDetails() {
		
		//Specify base URI
		
		RestAssured.baseURI="https://jsonplaceholder.typicode.com/todos/1";
		
		// Request object
		RequestSpecification httprequest = RestAssured.given();
		
		
		//Response object
		
		Response response = httprequest.request(Method.GET);
		
		//print response in console window
		String responsebody = response.getBody().asString();
		System.out.println(responsebody);
		
		Assert.assertEquals(responsebody.contains("delectus aut autem"),true);
		
	}
}
