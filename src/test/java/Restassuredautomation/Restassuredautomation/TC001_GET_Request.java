package Restassuredautomation.Restassuredautomation;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class TC001_GET_Request {
	
	
	@Test
	void getWeatherDetails() {
		
		//Specify base URI
		
		RestAssured.baseURI="https://jsonplaceholder.typicode.com/todos/1";
		
		// Request object
		RequestSpecification httprequest = RestAssured.given();
		
		
		//Response object
		
		Response response = httprequest.request(Method.GET);
		
		//print response in console window
		String responsebody = response.getBody().asString();
		System.out.println(responsebody);
		
		
		//status code validation
		int statuscode = response.statusCode();
		System.out.println(statuscode);
	    Assert.assertEquals(statuscode, 200);
	    
	    // status line verification
	    String statusLine = response.getStatusLine();
	    System.out.println(statusLine);
	    
	    Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	    
	    // Getting all headers from response
	    Headers allHeaders = response.headers();   // return all headres from response
	    
	    for(Header header:allHeaders) {
	    	
	    	System.out.println(header.getName());
	    	System.out.println(header.getValue());
	    }
	}

}
