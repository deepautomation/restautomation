package Restassuredautomation.Restassuredautomation;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import static org.hamcrest.core.IsEqual.*;
import static org.hamcrest.Matchers.*;
import org.hamcrest.collection.HasItemInArray;

import io.restassured.matcher.RestAssuredMatchers;
import org.apache.tools.ant.taskdefs.condition.Equals;
import static org.hamcrest.Matchers.equalTo;

public class Test_GET {

	@Test
	public void test_01() {
		
		given()
		.get("https://reqres.in/api/users?page=2")
		.then()
		.statusCode(200)
		.body("data.first_name", hasItems("Michael","Lindsay"))
		.log().all();
	}
}
